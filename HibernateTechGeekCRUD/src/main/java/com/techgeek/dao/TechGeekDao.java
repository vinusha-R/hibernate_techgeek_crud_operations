package com.techgeek.dao;

import java.util.List;
import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.techgeek.model.TechGeek;
import com.techgeek.model.TechGeekBlogs;
import com.techgeek.model.TechGeekName;
import com.techgeek.util.TechGeekSession;

public class TechGeekDao {
	Configuration con = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(TechGeek.class)
			.addAnnotatedClass(TechGeekBlogs.class);
	SessionFactory sf = con.buildSessionFactory();

	public void createGeek(TechGeek techGeek) {
		Session session = null;
		Transaction transaction = null;
		try {
			session = TechGeekSession.getSession();
			transaction = session.beginTransaction();
			List<TechGeekBlogs> techGeekBlogs = techGeek.getTechGeekBlogs();
			System.out.println(techGeekBlogs);
//			for(TechGeekBlogs techGeekBlog : techGeekBlogs) {
//				System.out.println(techGeekBlog);
//				session.save(techGeekBlog);
//			}
			for (int i = 0; i < techGeekBlogs.size(); i++) {
				session.save(techGeekBlogs.get(i));
			}
			session.save(techGeek);
			session.getTransaction().commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			try {
				if (session != null) {
					session.close();
				}
			} catch (Exception e) {
			}
		}
	}

	public TechGeek readGeek(int geekId) {
		Session session = null;
		Transaction transaction = null;
		TechGeek techGeek = null;
		try {
			session = TechGeekSession.getSession();
			transaction = session.beginTransaction();
			techGeek = (TechGeek) session.get(TechGeek.class, geekId);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (session != null) {
					session.close();
				}
			} catch (Exception e) {
			}

		}
		return techGeek;
	}

	public void updateGeek(TechGeek techGeek) {
		Session session = null;
		Transaction transaction = null;
		try {
			session = TechGeekSession.getSession();
			transaction = session.beginTransaction();
			session.saveOrUpdate(techGeek);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (session != null) {
					session.close();
				}
			} catch (Exception e) {
			}
		}
	}

	public void deleteGeek(TechGeek techGeek) {
		
		Session session = null;
		Transaction transaction = null;
		try {
			session = TechGeekSession.getSession();
			transaction = session.beginTransaction();
			for(int i=0;i<techGeek.getTechGeekBlogs().size();i++) {
				session.delete(techGeek.getTechGeekBlogs().get(i));
			}
			session.delete(techGeek);
			session.getTransaction().commit();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				if(session !=null) {
					session.close();
				}
			}catch(Exception e) {}
		}

	}
}