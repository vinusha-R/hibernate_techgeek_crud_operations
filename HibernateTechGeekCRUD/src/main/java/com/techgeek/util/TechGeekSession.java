package com.techgeek.util;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.techgeek.model.TechGeek;
import com.techgeek.model.TechGeekBlogs;

public class TechGeekSession {
	private static SessionFactory sessionFactory = null;
	static {
		try {
			loadSessionFactory();
		}catch(Exception e) {
			System.err.println("exception in initialization");
			e.printStackTrace();
		}
	}
		public static void loadSessionFactory() {
			Configuration con = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(TechGeek.class).addAnnotatedClass(TechGeekBlogs.class);
			sessionFactory = con.buildSessionFactory();
		}
	public static Session getSession() throws HibernateException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
		}catch(Throwable t) {
			System.out.println("exception in getting session");
			t.printStackTrace();
		}
		if(session == null) {
			System.out.println("session is not created. Its null");
		}
		return session;
	}	
}

		
		

