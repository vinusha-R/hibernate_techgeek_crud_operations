package com.techgeek.main;

import java.util.Scanner;

import com.techgeek.service.TechGeekOperations;

public class TechGeekMain {
	public static void main(String[] args) {
		while(true) {
			System.out.println("Choose your option");
			System.out.println("1:Create");
			System.out.println("2:Read");
			System.out.println("3:Update");
			System.out.println("4:Delete");
			System.out.println("5:exit");
			Scanner scanner = new Scanner(System.in); 
			int input = scanner.nextInt(); 
			TechGeekOperations geekOperations = new TechGeekOperations();
			switch(input) {
				case 1: geekOperations.create();
				break;
				case 2: geekOperations.read();
				break;
				case 3: geekOperations.update();
				break;
				case 4: geekOperations.delete();
				break;
				case 5: System.exit(0);
			}
		}
	}

}
