package com.techgeek.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class TechGeekBlogs {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int techGeekBlogId;
	private String blogName;
	private String blogDomain;
	
	@ManyToOne
	@JoinColumn(name = "techGeekId")
	private TechGeek techGeek;
	
	public TechGeek getTechGeek() {
		return techGeek;
	}
	public void setTechGeek(TechGeek techGeek) {
		this.techGeek = techGeek;
	}
	public int getTechGeekBlogId() {
		return techGeekBlogId;
	}
	public void setTechGeekBlogId(int techGeekBlogId) {
		this.techGeekBlogId = techGeekBlogId;
	}
	public String getBlogName() {
		return blogName;
	}
	public void setBlogName(String blogName) {
		this.blogName = blogName;
	}
	public String getBlogDomain() {
		return blogDomain;
	}
	public void setBlogDomain(String blogDomain) {
		this.blogDomain = blogDomain;
	}
	

}
