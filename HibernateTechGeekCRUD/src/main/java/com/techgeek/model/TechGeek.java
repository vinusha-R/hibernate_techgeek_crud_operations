package com.techgeek.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class TechGeek {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int techGeekId;
	private TechGeekName techGeekName;
	private String emailId;
	private String password;
	private String address;
	
	@OneToMany(mappedBy = "techGeek")
	private List<TechGeekBlogs> techGeekBlogs = new ArrayList<TechGeekBlogs>();
	
	public List<TechGeekBlogs> getTechGeekBlogs() {
		return techGeekBlogs;
	}
	public void setTechGeekBlogs(List<TechGeekBlogs> techGeekBlogs) {
		this.techGeekBlogs = techGeekBlogs;
	}
	public TechGeekName getTechGeekName() {
		return techGeekName;
	}
	public void setTechGeekName(TechGeekName techGeekName) {
		this.techGeekName = techGeekName;
	}
	public int getTechGeekId() {
		return techGeekId;
	}
	public void setTechGeekId(int techGeekId) {
		this.techGeekId = techGeekId;
	}
	
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
}
