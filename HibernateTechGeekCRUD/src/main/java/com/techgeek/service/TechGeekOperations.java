package com.techgeek.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.techgeek.dao.TechGeekDao;
import com.techgeek.model.TechGeek;
import com.techgeek.model.TechGeekBlogs;
import com.techgeek.model.TechGeekName;

public class TechGeekOperations {
	Scanner scanner = new Scanner(System.in);
	TechGeekDao techGeekDao = new TechGeekDao();
	public void create() {
		TechGeek techGeek = new TechGeek();
//		System.out.println("enter geek id");
//		int techGeekId = scanner.nextInt();
//		techGeek.setTechGeekId(techGeekId);
			System.out.println("enter geek first name");
			String firstName = scanner.next();
			System.out.println("enter geek middle name");
			String middleName = scanner.next();
			System.out.println("enter geek last name");
			String lastName = scanner.next();
			TechGeekName techGeekName = new TechGeekName();
			techGeekName.setFirstName(firstName);
			techGeekName.setMiddleName(middleName);
			techGeekName.setLastName(lastName);
		techGeek.setTechGeekName(techGeekName);
		System.out.println("enter geek mail id");
		String emailId = scanner.next();
		techGeek.setEmailId(emailId);
		System.out.println("enter geek password");
		String password = scanner.next();
		techGeek.setPassword(password);
		System.out.println("enter the geek address");
		String address = scanner.next();
		techGeek.setAddress(address);
		
		System.out.println("enter number of tech blogs");
		int blogCount = scanner.nextInt();
		List<TechGeekBlogs> geekBlogs = new ArrayList<TechGeekBlogs>();

		for(int i=0; i<blogCount;i++) {
			System.out.println("enter blog "+ i+1 +"details");
			TechGeekBlogs techGeekBlog = new TechGeekBlogs();
//			System.out.println("enter blog id");
//			int blogId = scanner.nextInt();
//			techGeekBlog.setTechGeekBlogId(blogId);
			System.out.println("enter blog name");
			String blogName = scanner.next();
			techGeekBlog.setBlogName(blogName);
			System.out.println("enter blog domain");
			String blogDomain = scanner.next();
			techGeekBlog.setBlogDomain(blogDomain);
			//techGeek.getTechGeekBlogs().add(techGeekBlog);
			techGeekBlog.setTechGeek(techGeek);
			geekBlogs.add(techGeekBlog);
		}
		techGeek.setTechGeekBlogs(geekBlogs);
		techGeekDao.createGeek(techGeek);
	}
	public void read() {
		System.out.println("enter geek id to get the details");
		int geekId = scanner.nextInt();
		TechGeek techGeek = techGeekDao.readGeek(geekId);
		System.out.println("your personal details are:");
		
		System.out.format("%10s%10s%10s%10s%10s%10s\n", "techGeekId", "firstName","middleName","lastName","emailId","password");
		System.out.println("-----------------------------------------------------------------");
			System.out.format("%10d%10s%10s%10s%10s%10s\n", techGeek.getTechGeekId(),techGeek.getTechGeekName().getFirstName(),techGeek.getTechGeekName().getMiddleName(),techGeek.getTechGeekName().getLastName(),techGeek.getEmailId(),techGeek.getPassword());
		List<TechGeekBlogs> geekblogs = techGeek.getTechGeekBlogs();
		System.out.println("your blog details are:");
		System.out.format("%10s%10s%10s\n","blogId","blogName","blogDomain");
		System.out.println("-------------------------------------------------------");
		for(TechGeekBlogs geekblog : geekblogs) {
			System.out.format("%10d%10s%10s\n",geekblog.getTechGeekBlogId(),geekblog.getBlogName(),geekblog.getBlogDomain());
		}
	}
	public void update() {
		System.out.println("enter the id you want to update the details of");
		int geekId = scanner.nextInt();
		TechGeek techGeek = techGeekDao.readGeek(geekId);
		System.out.println("enter the eamil id to update with");
		String emailId = scanner.next();
		techGeek.setEmailId(emailId);
		techGeekDao.updateGeek(techGeek);
	}
	public void delete() {
		System.out.println("enter the geek id you want to delete the details of");
		int geekId = scanner.nextInt();
		TechGeek techGeek = techGeekDao.readGeek(geekId);
		techGeekDao.deleteGeek(techGeek);
	}
}
